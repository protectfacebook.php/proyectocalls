package com.backend.callsBackend.domain.enumeration;

public enum TrainingType {
    TITULADA, COMPLEMENTARIA
}
