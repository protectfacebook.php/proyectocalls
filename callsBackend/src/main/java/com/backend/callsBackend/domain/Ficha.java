package com.backend.callsBackend.domain;

import com.backend.callsBackend.domain.enumeration.OfferType;
import com.backend.callsBackend.domain.enumeration.TrainingType;
import com.backend.callsBackend.domain.enumeration.WorkingDay;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;
import java.util.ArrayList;


@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Ficha {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;


    private String code;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;


    private boolean state;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateEndLective;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateEndPractice;

    @Enumerated(EnumType.STRING)
    private OfferType offerType;

    @Enumerated(EnumType.STRING)
    private TrainingType trainigType;

    @Enumerated(EnumType.STRING)
    private WorkingDay workingDay;

    private String  emailLastQuarter;


    @ManyToOne
    private Program program;
}
