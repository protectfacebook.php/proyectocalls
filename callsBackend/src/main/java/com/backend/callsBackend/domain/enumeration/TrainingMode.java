package com.backend.callsBackend.domain.enumeration;

public enum TrainingMode {

    VIRTUAL, PRESENCIAL
}
