package com.backend.callsBackend.domain;


import com.backend.callsBackend.domain.enumeration.RolUserFicha;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class TeamFicha {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private int id;

    @Enumerated(EnumType.STRING)
    private RolUserFicha rol;


    @OneToOne
    private Users users;


    @OneToOne
    private  Ficha ficha;
}
