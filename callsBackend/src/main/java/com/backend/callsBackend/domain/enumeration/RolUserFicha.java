package com.backend.callsBackend.domain.enumeration;

public enum RolUserFicha {
    GESTOR, INSTRUCTOR, PADRINO, LIDER_PADRINOS, APRENDIZ
}
