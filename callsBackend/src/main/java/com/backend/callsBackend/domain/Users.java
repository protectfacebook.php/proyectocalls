package com.backend.callsBackend.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Users {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private  long id;


    private String login;
    private String passwordHash;
    private String firstName;
    private String lastName;
    private String email;

    private String  imageUrl;
    private boolean activated;
    private String langKey;
    private String activationKey;
    private String resetKey;
    private String createdBy;


    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate createdDate;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate resetDate;


    private String lastModifiedBy;
    private String lastModifiedDate;
}
