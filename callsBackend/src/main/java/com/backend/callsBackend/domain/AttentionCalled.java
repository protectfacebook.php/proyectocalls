package com.backend.callsBackend.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class AttentionCalled {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long id;


    private String situationDescription;


    private String resumeSituation;


    private String impact;


    private String effect;


    private String commitment;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate creationDate;

    private boolean state;

    @ManyToOne
    private  Ficha ficha;

    @ManyToOne
    private  Users instructor;

    @ManyToOne
    private  Users apprentice; 
}
