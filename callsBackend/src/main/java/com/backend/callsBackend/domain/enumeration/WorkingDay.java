package com.backend.callsBackend.domain.enumeration;

public enum WorkingDay {
    MANIANA, TARDE, NOCHE, MIXTA
}
