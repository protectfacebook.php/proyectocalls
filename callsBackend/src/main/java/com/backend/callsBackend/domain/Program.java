package com.backend.callsBackend.domain;


import com.backend.callsBackend.domain.enumeration.TrainingMode;
import com.backend.callsBackend.domain.enumeration.TrainingType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Program {
    @Id
    @Length(max = 6)
    private String code;


    @Length(max = 4)
    private String version;


    @Length(max = 254)
    private  String nameProgram;


    private boolean status;

    private  int  durationTrimesterLective;

    @Length(min = 0 , max = 400)
    private String justification;

    private int  durationTrimesterProductive;

    @Enumerated(EnumType.STRING)
    private TrainingType trainingType;

    @Enumerated(EnumType.STRING)
    private TrainingMode trainingMode;



}
