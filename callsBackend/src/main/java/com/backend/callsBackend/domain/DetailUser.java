package com.backend.callsBackend.domain;


import com.backend.callsBackend.domain.enumeration.DocumentType;
import com.backend.callsBackend.domain.enumeration.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDate;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DetailUser {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private long id;

    @NotEmpty
    @Length(max = 12)
    private String documentNumber;

    private String phone;

    @Email
    private String emailPersonal;

    private String address;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate expeditionDate;



    private String initialsSchedule;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Enumerated(EnumType.STRING)
    private DocumentType documentType;


    private String emailSena;
    private boolean funcionario;
    private boolean contractTerminated;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateStartContract;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateEndContract;

    @OneToOne
    private Users user;
}
