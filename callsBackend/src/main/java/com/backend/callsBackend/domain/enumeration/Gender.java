package com.backend.callsBackend.domain.enumeration;

public enum Gender {
    MALE, FEMALE, OTHER
}
