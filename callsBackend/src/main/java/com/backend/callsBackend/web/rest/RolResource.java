package com.backend.callsBackend.web.rest;


import com.backend.callsBackend.domain.Rol;
import com.backend.callsBackend.service.IRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RolResource {

    @Autowired
    IRolService iRolService ;

    @GetMapping("/roles")
    public  Iterable<Rol> getAllRols(){
        return iRolService.getRols();
    }

    @PostMapping("/roles")
    public ResponseEntity createRol(@RequestBody Rol rol){
        return  iRolService.createRol(rol);
    }

    @DeleteMapping("/roles/{id}")
    public  String  deleteRol(@PathVariable Long id){
         return  iRolService.deleteRol(id);
    }

    @GetMapping("/rol-by-description")
    public Optional<Rol> getRolByDescription(@RequestParam(value = "description", required = false) String description){
        return  iRolService.getRolByDescription(description);
    }
}
