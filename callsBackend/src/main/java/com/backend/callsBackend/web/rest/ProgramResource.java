package com.backend.callsBackend.web.rest;


import com.backend.callsBackend.domain.Program;
import com.backend.callsBackend.service.IProgramService;
import com.backend.callsBackend.service.dto.ProgramDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProgramResource {
    @Autowired
    IProgramService programService;


    @PostMapping("/program")
    public ResponseEntity<ProgramDto> createProgram(@RequestBody ProgramDto programDto){
        return  programService.createProgram(programDto);
    }

    @GetMapping("/get-programs")
    public Page<ProgramDto> programsPaginated(@PathParam("pageNumber") Integer pageNumber,
                                              @PathParam("pageSize") Integer  pageSize){
        return  programService.readProgramsPaginate(pageNumber, pageSize);
    }

    @GetMapping("/programs")
    public  List<Program> getPrograms(){
        return programService.readAllPrograms();
    }
    @PutMapping("/program")
    public ResponseEntity<ProgramDto> updateProgram(@RequestBody ProgramDto programDto){
        return  programService.updateProgram(programDto);
    }

    @GetMapping("/program-name")
    public List<Program> readProgramName(@PathParam("nameProgram") String nameProgram){
        return  programService.getProgramByName(nameProgram);
    }
}













