package com.backend.callsBackend.web.rest;


import com.backend.callsBackend.domain.Ficha;
import com.backend.callsBackend.service.IFichaService;
import com.backend.callsBackend.service.dto.FichaDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class FichaResource {
    @Autowired
    IFichaService iFichaService;

    /*
    * this  rest  was created for creating the fichas */
    @PostMapping("/ficha")
    public ResponseEntity createFicha(@RequestBody  FichaDto fichaDto){
        return iFichaService.createFicha(fichaDto);
    }

    @GetMapping("/get-fichas")
    public Iterable<Ficha> getFichas(){
        return iFichaService.getAllFichas();
    }

    @GetMapping("/ficha")
    public Page<FichaDto> getAllFichas(@RequestParam(value = "pageNumber", required = false) Integer pageNumber,
                                       @RequestParam(value = "pageSize", required = false) Integer pageSize ){
        return iFichaService.getAllFichas(pageNumber,
                pageSize);
    }


    @PutMapping("/ficha")
    public ResponseEntity updateFicha(@RequestBody  FichaDto fichaDto){
        return iFichaService.update(fichaDto);
    }

    @GetMapping("ficha/search")
    public List<Ficha> getFichaByCode(@RequestParam(value = "code", required = false) String code ){
        return  iFichaService.getFichaByCode(code);
    }
}
