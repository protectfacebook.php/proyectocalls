package com.backend.callsBackend.web.rest;
import com.backend.callsBackend.domain.DetailUser;
import com.backend.callsBackend.service.IDetailUserService;
import com.backend.callsBackend.service.dto.DetailUserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class DetailUserResource {
    @Autowired
    IDetailUserService iDetailUserService;

    @PostMapping("/details")
    public ResponseEntity saveDetails(@RequestBody DetailUserDto detailUserDto){
        return  iDetailUserService.saveDetails(detailUserDto);
    }


    @GetMapping("/details")
    public Page<DetailUserDto> readDetails(@PathParam("pageNumber") Integer pageNumber,
                                           @PathParam("pageSize")  Integer pageSize ){
        return  iDetailUserService.getDetails(pageNumber, pageSize);
    }


    @GetMapping("/details-doc")
    public Iterable<DetailUser> findDetailsOfUserWithDocument(
            @PathParam("documentNumber")  String documentNumber){
        return  iDetailUserService.findDetailsOfUserWithDocumentContaining(documentNumber);
    }

    @GetMapping("/get-details")
    public  Iterable<DetailUser> findAllDetails(){
        return iDetailUserService.getAllDetails();
    }

    @GetMapping("/details-initials")
    public  Iterable<DetailUser> findByInitialsSchedule(
            @RequestParam(value = "initialsSchedule") String initialsSchedule){
        return  iDetailUserService.findByInitialsSchedule(initialsSchedule);
    }
}

