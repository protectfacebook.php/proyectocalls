package com.backend.callsBackend.web.rest;


import com.backend.callsBackend.domain.Users;
import com.backend.callsBackend.service.IServiceUser;
import com.backend.callsBackend.service.dto.UserDto;
import com.backend.callsBackend.service.dto.UserSafiDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class UsersResource {
    @Autowired
    IServiceUser iServiceUser;

    @PostMapping("/user")
    public ResponseEntity saveUser(@RequestBody UserDto userDto){
        return  iServiceUser.saveUser(userDto);
    }


    @PostMapping("/user-safi")
    public ResponseEntity<Users> saveUserSave(@RequestBody UserSafiDto userSafiDto){
        return  iServiceUser.saveUserSafi(userSafiDto);
    }
    @GetMapping("/get-users")
    public  Iterable<Users> getAllUsers(){
        return iServiceUser.getAllUsers();
    }

    @GetMapping("/user")
    public Page<UserDto> getUsers(@PathParam("pageNumber") Integer  pageNumber,
                                  @PathParam("pageSize") Integer pageSize){
        return  iServiceUser.readUser(pageNumber, pageSize);
    }

    @PutMapping("/user")
    public  UserDto updateUser(@RequestBody UserDto userDto){
        return  iServiceUser.updateUser(userDto);
    }

    @GetMapping("/user/search-by-login")
    public Optional<Users> findUserByLogin(@RequestParam(value = "login", required = false ) String login){
        return  iServiceUser.getByLogin(login);
    }




    @DeleteMapping("/user")
    public void deleteUserById(@PathVariable Integer id){
        iServiceUser.deleteUser(id);
    }

    @GetMapping("/user/firstName")
    public Iterable<Users> getUserByFirstName (@RequestParam(value = "firstName", required = false) String firstName){
        return  iServiceUser.getByFirstName(firstName);
    }

    @GetMapping("/user/firstNameAndLastName")
    public Iterable<Users> getUserByFirstNameAndLastName (@RequestParam(value = "firstName", required = false) String firstName,
                                                          @RequestParam(value = "lastName", required = false) String lastName){
        return  iServiceUser.getByFirstNameOrLastName(firstName, lastName);
    }
}
