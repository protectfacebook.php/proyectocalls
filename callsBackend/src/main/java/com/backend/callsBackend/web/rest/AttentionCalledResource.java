package com.backend.callsBackend.web.rest;

import com.backend.callsBackend.domain.AttentionCalled;
import com.backend.callsBackend.service.ILlamadoService;
import com.backend.callsBackend.service.dto.AttentionCalledDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class AttentionCalledResource {


    @Autowired
    ILlamadoService iLlamadoService;


    @PostMapping("/make-call")
    public ResponseEntity<AttentionCalledDto> save(@RequestBody AttentionCalledDto attentionCalledDto){
        return  iLlamadoService.createLlamado(attentionCalledDto);
    }


    @GetMapping("/get-calls")
    public  List<AttentionCalled> read(){
        return  iLlamadoService.getAllCalls();
    }

    @PutMapping("/remake-call")
    public ResponseEntity<AttentionCalledDto> updateCall(@RequestBody AttentionCalledDto attentionCalledDto){
        return  iLlamadoService.update(attentionCalledDto);
    }

    @GetMapping("/attention-user")
    public List<AttentionCalled> getCallsByUser(@RequestParam("documentNumber") String documentNumber){
        return iLlamadoService.getCallsByUser(documentNumber);
    }
}
