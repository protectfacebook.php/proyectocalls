package com.backend.callsBackend.repository;

import com.backend.callsBackend.domain.UserHasRol;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserHasRolRespository extends JpaRepository<UserHasRol, Long> {
}
