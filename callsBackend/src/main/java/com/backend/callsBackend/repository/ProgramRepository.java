package com.backend.callsBackend.repository;

import com.backend.callsBackend.domain.Program;
import com.backend.callsBackend.service.dto.ProgramDto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ProgramRepository extends JpaRepository<Program, String> {
    Optional<Program> findByCode (String code);
    List<Program> findByCodeContaining(String code);
    Optional<Program> findByNameProgram(String nameProgram);
    List<Program> findByNameProgramContaining(String nameProgram);

}
