package com.backend.callsBackend.repository;

import com.backend.callsBackend.domain.TeamFicha;

import org.springframework.data.jpa.repository.JpaRepository;


public interface TeamFichaRepository extends JpaRepository<TeamFicha, Integer> {

}
