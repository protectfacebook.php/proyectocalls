package com.backend.callsBackend.repository;

import com.backend.callsBackend.domain.AttentionCalled;
import com.backend.callsBackend.domain.UserHasRol;
import com.backend.callsBackend.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface LlamadoRepository extends JpaRepository<AttentionCalled, Long >  {
    List<AttentionCalled> findByApprentice (Users user );

}
