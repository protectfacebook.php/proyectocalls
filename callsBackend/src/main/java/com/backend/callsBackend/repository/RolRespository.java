package com.backend.callsBackend.repository;

import com.backend.callsBackend.domain.Rol;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RolRespository extends JpaRepository<Rol, Long > {


    Optional<Rol> findByDescription (String description);
}
