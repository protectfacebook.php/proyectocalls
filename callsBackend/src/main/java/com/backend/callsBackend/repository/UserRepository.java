package com.backend.callsBackend.repository;

import com.backend.callsBackend.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<Users, Integer> {
    Iterable<Users> findByLoginContaining (String login );
    Optional<Users> findByLogin (String login );
    Iterable<Users>findByFirstNameContaining (String firstName);
    Iterable<Users> findByFirstNameContainingOrLastNameContaining(String firstName, String LastName);
    Optional<Users> findByLoginAndPasswordHash (String email, String passwordHash);
}
