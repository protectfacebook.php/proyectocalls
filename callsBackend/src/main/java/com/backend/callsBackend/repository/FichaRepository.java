package com.backend.callsBackend.repository;

import com.backend.callsBackend.domain.Ficha;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface FichaRepository extends JpaRepository<Ficha, Integer> {
    Optional<Ficha> findByCode(String code);
    List<Ficha> findByCodeContaining (String code);
}
