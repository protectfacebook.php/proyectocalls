package com.backend.callsBackend.repository;

import com.backend.callsBackend.domain.DetailUser;

import com.backend.callsBackend.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface DetailUserRepository extends JpaRepository<DetailUser, Long > {
    Optional<DetailUser> findByUser (Users users);
    Optional<DetailUser> findByDocumentNumber (String documentNumber);
    Iterable<DetailUser> findByDocumentNumberContaining(String documentNumber);
    Optional<DetailUser> findByInitialsSchedule (String initialsSchedule);
    Iterable<DetailUser> findByInitialsScheduleContaining(String initialsSchedule);
}
