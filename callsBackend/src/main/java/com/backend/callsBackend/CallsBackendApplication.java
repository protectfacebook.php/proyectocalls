package com.backend.callsBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallsBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CallsBackendApplication.class, args);
	}

}
