package com.backend.callsBackend.service.transformer;


import com.backend.callsBackend.domain.Users;
import com.backend.callsBackend.service.dto.UserDto;

public class UsersTransformer {
    public static UserDto getUserDtoFromUsers(Users user){
        UserDto dto = new UserDto();

        dto.setId(user.getId());
        dto.setLogin(user.getLogin());
        dto.setPasswordHash(user.getPasswordHash());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setEmail(user.getEmail());
        dto.setActivated(false);
        dto.setImageUrl(user.getImageUrl());
        dto.setLangKey(user.getLangKey());
        dto.setActivationKey(user.getActivationKey());
        dto.setResetKey(user.getResetKey());
        dto.setResetDate(user.getResetDate());
        dto.setCreatedDate(user.getCreatedDate());
        return  dto;
    }

    public  static  Users getUserFromUserDto(UserDto userDto){
        Users user = new Users();
        user.setLogin(userDto.getLogin());
        user.setPasswordHash(userDto.getPasswordHash());

        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setEmail(userDto.getEmail());
        user.setActivated(true);
        user.setImageUrl(userDto.getImageUrl());
        user.setLangKey(userDto.getLangKey());
        user.setResetKey(userDto.getResetKey());
        user.setResetDate(userDto.getResetDate());
        user.setActivationKey(userDto.getActivationKey());
        user.setCreatedDate(userDto.getCreatedDate());

        return  user;
    }
}
