package com.backend.callsBackend.service.dto;

import com.backend.callsBackend.domain.Ficha;
import com.backend.callsBackend.domain.Users;
import com.backend.callsBackend.domain.enumeration.RolUserFicha;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;


@Getter
@Setter
public class TeamFichaDto  implements Serializable {
    private int id;
    private RolUserFicha rol;
    private Users users;
    private Ficha ficha;

}
