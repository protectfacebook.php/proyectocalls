package com.backend.callsBackend.service.dto;


import com.backend.callsBackend.domain.Users;
import com.backend.callsBackend.domain.enumeration.DocumentType;
import com.backend.callsBackend.domain.enumeration.Gender;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
public class DetailUserDto implements Serializable {
    private long id;
    private String documentNumber;
    private String phone;
    private String emailPersonal;
    private String address;
    private LocalDate expeditionDate;
    private String initialsSchedule;
    private Gender gender;
    private DocumentType documentType;
    private String emailSena;
    private boolean funcionario;
    private boolean contractTerminated;
    private LocalDate dateStartContract;
    private LocalDate dateEndContract;
    private Users users;
}
