package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.TeamFicha;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface ITeamFichaService {

    public ResponseEntity<TeamFicha> createTeamFicha(TeamFicha teamFicha);

    public Page<TeamFicha> readTeamFicha(Integer pageSize, Integer pageNumber );

    public  TeamFicha  update(TeamFicha teamFicha);

    public void deleteTeamFicha(Integer id);

    public Optional<TeamFicha> getById(Integer id);



}
