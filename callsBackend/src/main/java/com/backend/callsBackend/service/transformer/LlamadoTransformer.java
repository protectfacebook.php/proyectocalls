package com.backend.callsBackend.service.transformer;

import com.backend.callsBackend.domain.AttentionCalled;
import com.backend.callsBackend.service.dto.AttentionCalledDto;

public class LlamadoTransformer {
    public static AttentionCalledDto getLlamadoDtoFromLlamado(AttentionCalled attentionCalled){
        if (attentionCalled == null) {
            return null;
        }
        AttentionCalledDto dto = new AttentionCalledDto();
        dto.setFicha(attentionCalled.getFicha());
        dto.setId(attentionCalled.getId());
        dto.setCreationDate(attentionCalled.getCreationDate());
        dto.setSituationDescription(attentionCalled.getSituationDescription());
        dto.setResumeSituation(attentionCalled.getResumeSituation());
        dto.setImpact(attentionCalled.getImpact());
        dto.setEffect(attentionCalled.getEffect());
        dto.setState(attentionCalled.isState());
        dto.setInstructor(attentionCalled.getInstructor());
        dto.setApprentice(attentionCalled.getApprentice());
        dto.setCommitment(attentionCalled.getCommitment());
        return dto;
    }
    public static AttentionCalled getLlamadoFromLlamadoDto(AttentionCalledDto dto){
        if (dto == null){
            return null;
        }
        AttentionCalled attentionCalled = new AttentionCalled();
        attentionCalled.setId(attentionCalled.getId());
        attentionCalled.setFicha(dto.getFicha());
        attentionCalled.setSituationDescription(dto.getSituationDescription());
        attentionCalled.setResumeSituation(dto.getResumeSituation());
        attentionCalled.setImpact(dto.getImpact());
        attentionCalled.setEffect(dto.getEffect());
        attentionCalled.setCommitment(dto.getCommitment());
        attentionCalled.setState(dto.isState());
        attentionCalled.setInstructor(dto.getInstructor());
        attentionCalled.setApprentice(dto.getApprentice());
        attentionCalled.setCreationDate(dto.getCreationDate());
        return attentionCalled;
    }
}
