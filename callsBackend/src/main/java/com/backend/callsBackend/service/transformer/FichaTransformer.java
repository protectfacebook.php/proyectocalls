package com.backend.callsBackend.service.transformer;

import com.backend.callsBackend.domain.Ficha;
import com.backend.callsBackend.service.dto.FichaDto;

public class FichaTransformer {
    public static FichaDto getFichaDtoFromFicha(Ficha ficha){
        if(ficha == null){
            return null;
        }
        FichaDto dto = new FichaDto();
        dto.setId(ficha.getId());
        dto.setCode(ficha.getCode());
        dto.setStartDate(ficha.getStartDate());
        dto.setState(ficha.isState());
        dto.setDateEndLective(ficha.getDateEndLective());
        dto.setDateEndPractice(ficha.getDateEndPractice());
        dto.setOfferType(ficha.getOfferType());
        dto.setTrainigType(ficha.getTrainigType());
        dto.setWorkingDay(ficha.getWorkingDay());
        dto.setEmailLastQuarter(ficha.getEmailLastQuarter());
        dto.setProgram(ficha.getProgram());
        return dto;
    };

    public  static  Ficha getFichaFromFichaDto(FichaDto fichaDto){
        if(fichaDto == null){

        }
        Ficha ficha = new Ficha();

        ficha.setCode(fichaDto.getCode());
        ficha.setStartDate(fichaDto.getStartDate());
        ficha.setProgram(fichaDto.getProgram());
        ficha.setState(fichaDto.isState());
        ficha.setDateEndLective(fichaDto.getDateEndLective());
        ficha.setOfferType(fichaDto.getOfferType());
        ficha.setDateEndPractice(fichaDto.getDateEndPractice());
        ficha.setTrainigType(fichaDto.getTrainigType());
        ficha.setEmailLastQuarter(fichaDto.getEmailLastQuarter());
        ficha.setWorkingDay(fichaDto.getWorkingDay());
        return  ficha;
    }
}
