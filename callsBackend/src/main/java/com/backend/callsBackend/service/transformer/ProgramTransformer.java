package com.backend.callsBackend.service.transformer;

import com.backend.callsBackend.domain.Program;
import com.backend.callsBackend.service.dto.ProgramDto;

public class ProgramTransformer {


    public static ProgramDto getProgramDtoFromProgram(Program program){
        if (program == null){
            return null;
        }
        ProgramDto dto = new ProgramDto();
        dto.setCode(program.getCode());
        dto.setVersion(program.getVersion());
        dto.setNameProgram(program.getNameProgram());
        dto.setDurationTrimesterLective(program.getDurationTrimesterLective());
        dto.setDurationTrimesterProductive(program.getDurationTrimesterProductive());
        dto.setJustification(program.getJustification());
        dto.setTrainingMode(program.getTrainingMode());
        dto.setStatus(program.isStatus());
        dto.setTrainingType(program.getTrainingType());
        return dto;
    }
    public static Program getProgramFromProgramDto(ProgramDto programDto){
        if (programDto == null){
            return null;
        }
        Program program = new Program();
        program.setCode(programDto.getCode());
        program.setVersion(programDto.getVersion());
        program.setNameProgram(programDto.getNameProgram());
        program.setDurationTrimesterLective(programDto.getDurationTrimesterLective());
        program.setJustification(programDto.getJustification());
        program.setDurationTrimesterProductive(programDto.getDurationTrimesterProductive());
        program.setTrainingMode(programDto.getTrainingMode());
        program.setTrainingType(programDto.getTrainingType());
        program.setStatus(programDto.isStatus());
        return  program;
    }
}
