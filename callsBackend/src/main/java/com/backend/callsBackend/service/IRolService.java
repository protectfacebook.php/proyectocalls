package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.Rol;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IRolService {

    public ResponseEntity<Rol> createRol (Rol rol);

    public  Iterable<Rol> getRols();

    public Optional<Rol> getRolByDescription(String description);

    public  String  deleteRol(Long  id );
}
