package com.backend.callsBackend.service;


import com.backend.callsBackend.domain.Rol;
import com.backend.callsBackend.repository.RolRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IRolServiceImp implements  IRolService {
    @Autowired
    RolRespository rolRespository;

    @Override
    public ResponseEntity<Rol> createRol(Rol rol) {
        if (rolRespository.findByDescription(rol.getDescription()).isPresent()){
            return new ResponseEntity("Este rol ya existe", HttpStatus.BAD_REQUEST);
        }{
            return new ResponseEntity(rolRespository.save(rol), HttpStatus.OK);
        }
    }

    @Override
    public Iterable<Rol> getRols() {
        return rolRespository.findAll();
    }

    @Override
    public Optional<Rol> getRolByDescription(String description) {
        return rolRespository.findByDescription(description);
    }

    @Override
    public String deleteRol(Long  id) {
        rolRespository.deleteById(id);
        return "Rol eliminado";
    }
}
