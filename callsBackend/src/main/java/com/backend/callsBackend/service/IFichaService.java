package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.Ficha;
import com.backend.callsBackend.service.dto.FichaDto;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface IFichaService {

    public ResponseEntity<FichaDto> createFicha(FichaDto fichaDto);

    public Page<FichaDto> getAllFichas(Integer pageNumber, Integer pageSize);

    public ResponseEntity<FichaDto> update(FichaDto fichaDto);

    public  Iterable<Ficha> getAllFichas();

    public List<Ficha> getFichaByCode(String code);


}
