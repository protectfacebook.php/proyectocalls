package com.backend.callsBackend.service.transformer;

import com.backend.callsBackend.domain.UserHasRol;
import com.backend.callsBackend.service.dto.UserHasRolDto;

public class UserHasRolTransformer {

    public  static UserHasRolDto getUserHasRolDtoFromUserHasRol(UserHasRol userHasRol){
        UserHasRolDto dto = new UserHasRolDto();
        dto.setId(userHasRol.getId());
        dto.setRol(userHasRol.getRol());
        dto.setUser(userHasRol.getUser());
        return dto;
    }
    public static  UserHasRol getUserHasRolFromUserHasRolDto(UserHasRolDto userHasRolDto){
        UserHasRol userHasRol = new UserHasRol();
        userHasRol.setRol(userHasRolDto.getRol());
        userHasRol.setUser(userHasRolDto.getUser());

        return  userHasRol;
    }
}
