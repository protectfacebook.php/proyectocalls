package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.Users;
import com.backend.callsBackend.service.dto.UserDto;
import com.backend.callsBackend.service.dto.UserSafiDto;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface IServiceUser {

    public ResponseEntity<Users> saveUserSafi(UserSafiDto userSafiDto);

    public ResponseEntity<UserDto>  saveUser(UserDto usersDto);

    public Page<UserDto> readUser(Integer pageNumber  , Integer pageSize  );

    public UserDto updateUser(UserDto userDto );

    public Iterable<Users> getAllUsers();

    public void deleteUser(Integer id);

    public Optional<Users> getById(Integer id);


    public Optional<Users> getByLogin(String login);

    public  Iterable<Users> getByFirstName(String  firstName );
    public  Iterable<Users> getByFirstNameOrLastName(String  firstName, String LastName  );
}
