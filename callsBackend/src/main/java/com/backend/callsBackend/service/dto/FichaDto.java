package com.backend.callsBackend.service.dto;

import com.backend.callsBackend.domain.Program;
import com.backend.callsBackend.domain.enumeration.OfferType;
import com.backend.callsBackend.domain.enumeration.TrainingType;
import com.backend.callsBackend.domain.enumeration.WorkingDay;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
public class FichaDto implements Serializable {
    private int id;

    private String code;
    private LocalDate startDate;
    private boolean state;
    private LocalDate dateEndLective;
    private LocalDate dateEndPractice;
    private OfferType offerType;
    private TrainingType trainigType;
    private WorkingDay workingDay;
    private String emailLastQuarter;
    private Program program;
}









