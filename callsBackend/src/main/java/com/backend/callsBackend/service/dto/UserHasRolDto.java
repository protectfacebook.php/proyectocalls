package com.backend.callsBackend.service.dto;

import com.backend.callsBackend.domain.Rol;
import com.backend.callsBackend.domain.Users;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;



public class UserHasRolDto implements Serializable {
    private long id;

    private Rol rol;
    private Users user;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Rol getRol() {
        return rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }
}
















