package com.backend.callsBackend.service.transformer;

import com.backend.callsBackend.domain.TeamFicha;
import com.backend.callsBackend.service.dto.TeamFichaDto;

public class TeamFichaTransformer {
    public static TeamFichaDto getTeamFichaDtoFromTeamFicha(TeamFicha teamFicha) {
        if(teamFicha == null){
            return null;
        }
        TeamFichaDto dto = new TeamFichaDto();
        dto.setId(teamFicha.getId());
        dto.setRol(teamFicha.getRol());
        dto.setUsers(teamFicha.getUsers());
        dto.setFicha(teamFicha.getFicha());
        return dto;
    }
    public static TeamFicha getTeamFichaFromTeamFichaDto(TeamFichaDto dto){
        if (dto == null){
            return null;
        }
        TeamFicha teamFicha = new TeamFicha();
        teamFicha.setId(dto.getId());
        teamFicha.setRol(dto.getRol());
        teamFicha.setUsers(dto.getUsers());
        teamFicha.setFicha(dto.getFicha());
        return teamFicha;
    }
}
