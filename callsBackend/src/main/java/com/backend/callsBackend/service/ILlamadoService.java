package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.AttentionCalled;
import com.backend.callsBackend.service.dto.AttentionCalledDto;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface ILlamadoService {



    public ResponseEntity<AttentionCalledDto> createLlamado(AttentionCalledDto attentionCalledDto);



    public Page<AttentionCalledDto> readLlamado(Integer pageSize, Integer pageNumber);

    public ResponseEntity<AttentionCalledDto> update(AttentionCalledDto attentionCalledDto);

    public void delete(Long  id);

    public  List<AttentionCalled> getAllCalls();

    public Optional<AttentionCalled> getById(Long  id);

    public List<AttentionCalled> getCallsByUser(String documentNumber);

}
