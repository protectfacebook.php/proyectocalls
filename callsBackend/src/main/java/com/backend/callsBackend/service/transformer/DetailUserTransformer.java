package com.backend.callsBackend.service.transformer;

import com.backend.callsBackend.domain.DetailUser;
import com.backend.callsBackend.service.dto.DetailUserDto;

public class DetailUserTransformer {
    public static DetailUserDto getDetailUserDtoFromDetailUser(DetailUser detailUser){
        if (detailUser == null){
            return  null;
        }else {
            DetailUserDto detailUserDto = new DetailUserDto();
            detailUserDto.setDocumentNumber(detailUser.getDocumentNumber());
            detailUserDto.setPhone(detailUser.getPhone());
            detailUserDto.setEmailPersonal(detailUser.getEmailPersonal());
            detailUserDto.setAddress(detailUser.getAddress());
            detailUserDto.setExpeditionDate(detailUser.getExpeditionDate());
            detailUserDto.setUsers(detailUser.getUser());
            detailUserDto.setInitialsSchedule(detailUser.getInitialsSchedule());
            detailUserDto.setGender(detailUser.getGender());
            detailUserDto.setDocumentType(detailUser.getDocumentType());
            detailUserDto.setEmailSena(detailUser.getEmailSena());
            detailUserDto.setFuncionario(detailUser.isFuncionario());
            detailUserDto.setContractTerminated(detailUser.isContractTerminated());
            detailUserDto.setDateStartContract(detailUser.getDateStartContract());
            detailUserDto.setDateEndContract(detailUser.getDateEndContract());
            return  detailUserDto;
        }
    }
    public static DetailUser getDetailUserFromDetailUserDto(DetailUserDto detailUserDto){
        if (detailUserDto == null){
            return  null;
        }
        DetailUser detailUser = new DetailUser();
        detailUser.setUser(detailUserDto.getUsers());
        detailUser.setDocumentNumber(detailUserDto.getDocumentNumber());
        detailUser.setPhone(detailUserDto.getPhone());
        detailUser.setEmailPersonal(detailUserDto
        .getEmailPersonal());
        detailUser.setAddress(detailUserDto.getAddress());
        detailUser.setExpeditionDate(detailUserDto.getExpeditionDate());
        detailUser.setInitialsSchedule(detailUserDto.getInitialsSchedule());
        detailUser.setDocumentType(detailUserDto.getDocumentType());
        detailUser.setGender(detailUserDto.getGender());
        detailUser.setEmailSena(detailUserDto.getEmailSena());
        detailUser.setFuncionario(detailUserDto.isFuncionario());
        detailUser.setContractTerminated(detailUserDto.isContractTerminated());
        detailUser.setDateStartContract(detailUserDto.getDateStartContract());
        detailUser.setDateEndContract(detailUserDto.getDateEndContract());
        return  detailUser;
    }
}
