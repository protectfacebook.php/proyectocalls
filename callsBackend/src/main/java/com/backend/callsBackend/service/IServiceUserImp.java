package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.DetailUser;
import com.backend.callsBackend.domain.Rol;
import com.backend.callsBackend.domain.UserHasRol;
import com.backend.callsBackend.domain.Users;
import com.backend.callsBackend.repository.DetailUserRepository;
import com.backend.callsBackend.repository.UserHasRolRespository;
import com.backend.callsBackend.repository.UserRepository;

import com.backend.callsBackend.service.dto.UserDto;
import com.backend.callsBackend.service.dto.UserHasRolDto;
import com.backend.callsBackend.service.dto.UserSafiDto;
import com.backend.callsBackend.service.transformer.UserHasRolTransformer;
import com.backend.callsBackend.service.transformer.UsersTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Optional;
import java.util.Random;


@Service
public class IServiceUserImp implements IServiceUser {
    @Autowired
    UserRepository userRepository;

    @Autowired
    DetailUserRepository detailUserRepository;

    @Autowired
    UserHasRolRespository userHasRolRespository;
    /**
     * this method create a random key of letters */
    public String randomKey(){
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(20);
        Random random = new Random();
        for (int i = 0; i < 20; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return  sb.toString();
    }

    /**
     * this method  is for saving an user with their details in an only send */
    @Override
    public ResponseEntity<Users> saveUserSafi(UserSafiDto userSafiDto) {
        Users user = new Users();
        user.setLogin(userSafiDto.getLogin());
        if (userRepository.findByLogin(user.getLogin()).isPresent()){
            return new ResponseEntity("Login existente", HttpStatus.BAD_REQUEST);

        }else {
            DetailUser detailUser = new DetailUser();
            detailUser.setDocumentNumber(userSafiDto.getDocumentNumber());
            if (detailUserRepository.findByDocumentNumber(detailUser.getDocumentNumber()).isPresent()){
                return new ResponseEntity("Este documento ya existe", HttpStatus.BAD_REQUEST);
            }else {
                user.setLangKey("ES");
                user.setFirstName(userSafiDto.getFirstName());
                user.setLastName(userSafiDto.getLastName());
                if (userSafiDto.getEmail() != null) {
                    user.setEmail(userSafiDto
                            .getEmail().toLowerCase());
                }
                user.setImageUrl(userSafiDto.getImageUrl());
                user.setPasswordHash(userSafiDto.getDocumentNumber());
                user.setActivated(true);
                user.setActivationKey(this.randomKey());
                Users user1 = userRepository.save(user);
                detailUser.setAddress(userSafiDto.getAddress());
                detailUser.setEmailPersonal(userSafiDto.getEmailPersonal());
                detailUser.setEmailSena(userSafiDto.getEmail());
                detailUser.setDocumentType(userSafiDto.getDocumentType());
                detailUser.setExpeditionDate(userSafiDto.getExpeditionDate());
                detailUser.setGender(userSafiDto.getGender());
                detailUser.setInitialsSchedule(userSafiDto.getInitialsSchedule());
                detailUser.setPhone(userSafiDto.getPhone());
                detailUser.setUser(user1);
                detailUserRepository.save(detailUser);
                return new ResponseEntity("usuario creado " + user, HttpStatus.OK);
            }
        }


    }
    /**
     * this method save an user
     * */
    @Override
    public ResponseEntity<UserDto> saveUser(UserDto usersDto) {

            Users user = UsersTransformer.getUserFromUserDto(usersDto);
            //here is verified that the login entry not exist in  the data base
           if (userRepository.findByLogin(user.getLogin()).isPresent()){
               return  new ResponseEntity("this login already exist ", HttpStatus.BAD_REQUEST);

           }else {

               user.setActivated(true);
               user.setActivationKey(this.randomKey());
               user.setCreatedDate(LocalDate.now());
               if (usersDto.getRol() != null){
                   Rol rol = usersDto.getRol();
                   UserHasRolDto userHasRolDto = new UserHasRolDto();
                   userHasRolDto.setUser(user);
                   userHasRolDto.setRol(rol);
                   UserHasRol userHasRol = UserHasRolTransformer.getUserHasRolFromUserHasRolDto(userHasRolDto);
                   userHasRolRespository.save(userHasRol);
               }
               return new ResponseEntity(UsersTransformer.getUserDtoFromUsers(userRepository.save(user)) , HttpStatus.OK);
           }
    }

    /**
     * this method return all users in pages */
    @Override
    public Page<UserDto> readUser(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return userRepository.findAll(pageable).map(UsersTransformer::getUserDtoFromUsers);
    }



    /**
     * this method is for updating an existing user */
    @Override
    public UserDto updateUser(UserDto userDto) {
        Users user = UsersTransformer.getUserFromUserDto(userDto);
        return UsersTransformer.getUserDtoFromUsers(userRepository.save(user));
    }

    @Override
    public Iterable<Users> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUser(Integer id) {
       userRepository.deleteById(id);
    }

    @Override
    public Optional<Users> getById(Integer id) {
        return userRepository.findById(id);
    }



    @Override
    public Optional<Users> getByLogin(String login) {
        return  userRepository.findByLogin(login);
    }

    @Override
    public Iterable<Users> getByFirstName(String firstName) {
        return userRepository.findByFirstNameContaining(firstName);
    }



    @Override
    public Iterable<Users> getByFirstNameOrLastName(String firstName, String LastName) {
        return userRepository.findByFirstNameContainingOrLastNameContaining(firstName, LastName);
    }
}
