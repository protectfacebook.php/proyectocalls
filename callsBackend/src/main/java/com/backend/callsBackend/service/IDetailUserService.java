package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.DetailUser;

import com.backend.callsBackend.service.dto.DetailUserDto;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface  IDetailUserService  {
    public ResponseEntity<DetailUserDto> saveDetails(DetailUserDto detailUserDto);

    public Page<DetailUserDto> getDetails(Integer pageNumber , Integer pageSize );

    public Iterable<DetailUser> findDetailsOfUserWithDocumentContaining(String documentNumber);

    public Iterable<DetailUser> getAllDetails();

    public  Iterable<DetailUser> findByInitialsSchedule(String initialsSchedule);

}
