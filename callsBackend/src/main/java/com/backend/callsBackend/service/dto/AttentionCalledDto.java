package com.backend.callsBackend.service.dto;

import com.backend.callsBackend.domain.Ficha;
import com.backend.callsBackend.domain.Users;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDate;


@Getter
@Setter
public class AttentionCalledDto implements Serializable {
    private Long id;
    private Users instructor;
    private Users apprentice;
    private LocalDate creationDate;
    private Ficha ficha;
    private boolean state;
    private String situationDescription;
    private String resumeSituation;
    private String impact;
    private String effect;
    private String commitment;
}
