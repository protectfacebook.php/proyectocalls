package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.Ficha;
import com.backend.callsBackend.domain.TeamFicha;
import com.backend.callsBackend.repository.TeamFichaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class ITeamFichaServiceImp implements ITeamFichaService{

    @Autowired
    TeamFichaRepository teamFichaRepository;

    /*
    @Override
    public ResponseEntity<TeamFicha> createTeamFicha(TeamFicha teamFicha) {
        if (teamFichaRepository.finByUser(teamFicha.getUser()).isPresent()){
            return new ResponseEntity("el usuario ya existe", HttpStatus.BAD_REQUEST);
        }else{
            return new ResponseEntity(teamFichaRepository.save(teamFicha), HttpStatus.OK);
        }
    }
 */

    @Override
    public ResponseEntity<TeamFicha> createTeamFicha(TeamFicha teamFicha) {
        return new ResponseEntity(teamFichaRepository.save(teamFicha), HttpStatus.OK);
    }

    @Override
    public Page<TeamFicha> readTeamFicha(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of(pageSize, pageNumber);
        return teamFichaRepository.findAll(pageable) ;
    }

    @Override
    public TeamFicha update(TeamFicha teamFicha) {
        return teamFichaRepository.save(teamFicha);
    }

    @Override
    public void deleteTeamFicha(Integer id) {
    teamFichaRepository.deleteById(id);
    }

    @Override
    public Optional<TeamFicha> getById(Integer id) {
        return teamFichaRepository.findById(id);
    }
}
