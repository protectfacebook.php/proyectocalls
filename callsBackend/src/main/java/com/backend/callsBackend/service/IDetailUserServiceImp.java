package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.DetailUser;
import com.backend.callsBackend.repository.DetailUserRepository;

import com.backend.callsBackend.service.dto.DetailUserDto;
import com.backend.callsBackend.service.transformer.DetailUserTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class IDetailUserServiceImp implements  IDetailUserService {
    @Autowired
    DetailUserRepository detailUserRepository;


    /**
     * This method is used for saving the details of an user, this work with an input  of detailsDto  type
     * inside of detailsDto also is present the user_id of the user to save
     * */
    @Override
    public ResponseEntity<DetailUserDto> saveDetails(DetailUserDto detailUserDto) {
        DetailUser detailUser = DetailUserTransformer.getDetailUserFromDetailUserDto(detailUserDto);
        detailUser.setEmailSena(detailUserDto.getUsers().getEmail());
       return  new ResponseEntity<DetailUserDto>(DetailUserTransformer.getDetailUserDtoFromDetailUser(detailUserRepository.save(detailUser)), HttpStatus.OK);
    }

    /*
     * this method  is used to find all details of users   with their  correspondent pagination
     * here there is two inputs, pageNumber corresponding to the number of the page to watch
     * and pageSize is the quantity of details for page  */
    @Override
    public Page<DetailUserDto> getDetails(Integer pageSize, Integer pageNumber) {
        return null;
    }

    /**
     * this method search an user with their document number
     * */
    @Override
    public Iterable<DetailUser> findDetailsOfUserWithDocumentContaining(String documentNumber) {
        return  detailUserRepository.findByDocumentNumberContaining(documentNumber);
    }

    @Override
    public Iterable<DetailUser> getAllDetails() {
        return detailUserRepository.findAll();
    }

    /**
     * this method is used to find an user with their initialsSchedule
     * */
    @Override
    public Iterable<DetailUser> findByInitialsSchedule(String initialsSchedule) {
        return detailUserRepository.findByInitialsScheduleContaining(initialsSchedule);
    }


}
