package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.Program;
import com.backend.callsBackend.service.dto.ProgramDto;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

public interface IProgramService {

    public ResponseEntity<ProgramDto> createProgram(ProgramDto programDto);

    public Page<ProgramDto> readProgramsPaginate(Integer pageNumber, Integer pageSize);

    public List<Program> readAllPrograms();

    public ResponseEntity<ProgramDto> updateProgram(ProgramDto programDto);

    public List<Program> getProgramByName(String nameProgram);
}
