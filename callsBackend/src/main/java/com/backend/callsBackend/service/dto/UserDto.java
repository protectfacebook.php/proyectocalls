package com.backend.callsBackend.service.dto;

import com.backend.callsBackend.domain.Rol;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;


@Getter
@Setter
public class UserDto implements Serializable {


    private Long id ;

    @NotBlank
    @Size(min = 1, max = 50)
    private String login;

    @Size(max = 50)
    private String firstName;

    @Size(max = 50)
    private String lastName;

    private String passwordHash;

    private String activationKey;
    private String resetKey;
    @Email
    @Size(min = 5, max = 254)
    private String email;

    @Size(max = 256)
    private String imageUrl;

    private boolean activated = true;

    @Size(min = 2, max = 10)
    private String langKey;

    private String createdBy;


    private LocalDate createdDate;


    private LocalDate resetDate;

    private Rol rol;

    private String lastModifiedBy;

    private Instant lastModifiedDate;
}
