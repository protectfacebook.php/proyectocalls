package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.Ficha;
import com.backend.callsBackend.repository.FichaRepository;
import com.backend.callsBackend.service.dto.FichaDto;
import com.backend.callsBackend.service.transformer.FichaTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IFichaServiceImp implements IFichaService {

    @Autowired
    FichaRepository fichaRepository;


    /**
     * This method is used for saving a ficha, this work with an input  of fichaDto type
     * */
    @Override
    public ResponseEntity<FichaDto> createFicha(FichaDto fichaDto) {
        Ficha ficha = FichaTransformer.getFichaFromFichaDto(fichaDto);
        if(fichaRepository.findByCode(ficha.getCode()).isPresent()){
            return  new ResponseEntity("Este codigo de ficha ya existe", HttpStatus.BAD_REQUEST);
        }else {
            return  new ResponseEntity<>(FichaTransformer.getFichaDtoFromFicha(fichaRepository.save(ficha)), HttpStatus.OK);
        }
    }

    /*
     * this method  is used to find all fichas with their  correspondent pagination
     * here there is two inputs, pageNumber corresponding to the number of the page to watch
     * and pageSize is the quantity of fichas for page  */
    @Override
    public Page<FichaDto> getAllFichas(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return  fichaRepository.findAll(pageable)
                .map(FichaTransformer::getFichaDtoFromFicha);
    }


    /**
     * this method is for updating the existing fichas
     * */
    @Override
    public ResponseEntity<FichaDto> update(FichaDto fichaDto) {
        Ficha ficha = FichaTransformer.getFichaFromFichaDto(fichaDto);
        if (fichaRepository.findByCode(ficha.getCode()).isPresent()){
            return new  ResponseEntity<>(FichaTransformer.getFichaDtoFromFicha(fichaRepository.save(ficha)), HttpStatus.OK);
        }else {
            return  new ResponseEntity("Ésta ficha no esxiste ", HttpStatus.BAD_REQUEST);
        }
    }

    @Override
    public Iterable<Ficha> getAllFichas() {
        return fichaRepository.findAll();
    }

    /**
     * this method is for searching  a ficha with their code and after to search return the ficha*/

    @Override
    public List<Ficha> getFichaByCode(String code) {
        return fichaRepository.findByCodeContaining(code);
    }
}


