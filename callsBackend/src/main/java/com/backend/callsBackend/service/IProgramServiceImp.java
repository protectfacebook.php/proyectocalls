package com.backend.callsBackend.service;


import com.backend.callsBackend.domain.Program;
import com.backend.callsBackend.repository.ProgramRepository;
import com.backend.callsBackend.service.dto.ProgramDto;
import com.backend.callsBackend.service.transformer.ProgramTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class IProgramServiceImp implements  IProgramService {
    @Autowired
    ProgramRepository programRepository;

    @Override
    public ResponseEntity<ProgramDto> createProgram(ProgramDto programDto) {
        Program program = ProgramTransformer.getProgramFromProgramDto(programDto);
        if (programRepository.findByCode(program.getCode()).isPresent()){
            return new ResponseEntity("El codigo del programa ya existe", HttpStatus.BAD_REQUEST);
        }else {
            return new ResponseEntity(ProgramTransformer.getProgramDtoFromProgram(programRepository.save(program)), HttpStatus.OK);
        }
    }

    @Override
    public Page<ProgramDto> readProgramsPaginate(Integer pageNumber, Integer pageSize) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        return  programRepository.findAll(pageable)
                .map(ProgramTransformer::getProgramDtoFromProgram);
    }

    @Override
    public List<Program> readAllPrograms() {
        return  programRepository.findAll();
    }

    @Override
    public ResponseEntity<ProgramDto> updateProgram(ProgramDto programDto) {
        Program program = ProgramTransformer.getProgramFromProgramDto(programDto);
        return new ResponseEntity(ProgramTransformer.getProgramDtoFromProgram(programRepository.save(program)), HttpStatus.OK);
    }

    @Override
    public List<Program> getProgramByName(String nameProgram) {
        return programRepository.findByNameProgramContaining(nameProgram);
    }
}
