package com.backend.callsBackend.service.dto;

import com.backend.callsBackend.domain.enumeration.TrainingMode;
import com.backend.callsBackend.domain.enumeration.TrainingType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Id;
import java.io.Serializable;

@Getter
@Setter
public class ProgramDto implements Serializable {

    @Length(max = 6)
    private String code;


    @Length(max = 4)
    private String version;


    @Length(max = 254)
    private  String nameProgram;

    private  int  durationTrimesterLective;

    private boolean status;

    @Length(min = 0 , max = 400)
    private String justification;

    private int  durationTrimesterProductive;
    private TrainingType trainingType;
    private TrainingMode trainingMode;

}
