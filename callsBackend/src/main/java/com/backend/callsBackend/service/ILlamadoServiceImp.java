package com.backend.callsBackend.service;

import com.backend.callsBackend.domain.AttentionCalled;
import com.backend.callsBackend.domain.DetailUser;
import com.backend.callsBackend.domain.Users;
import com.backend.callsBackend.repository.DetailUserRepository;
import com.backend.callsBackend.repository.LlamadoRepository;
import com.backend.callsBackend.service.dto.AttentionCalledDto;
import com.backend.callsBackend.service.transformer.LlamadoTransformer;
import com.backend.callsBackend.utils.SendMailer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;


@Service
public class ILlamadoServiceImp implements ILlamadoService {

    @Autowired
    LlamadoRepository llamadoRepository;

    @Autowired
    DetailUserRepository detailUserRepository;

    @Autowired
    JavaMailSender javaMailSender;
    /**
     * This method is for saving a attentionCalled ,
     * this method wait how input two users, the instructor and apprentice,
     * this method return a ResponseEntity*/
    @Override
    public ResponseEntity<AttentionCalledDto> createLlamado(AttentionCalledDto attentionCalledDto) {
        AttentionCalled attentionCalled = LlamadoTransformer.getLlamadoFromLlamadoDto(attentionCalledDto);
        attentionCalled.setCreationDate(LocalDate.now());
        sendMail(attentionCalled.getApprentice().getEmail());
        return new ResponseEntity(LlamadoTransformer.getLlamadoDtoFromLlamado(llamadoRepository.save(attentionCalled)), HttpStatus.OK);
    }
    public  void sendMail( String to){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject("Tienes un nuevo llamado de atencion");
        message.setText("Hola, tienes un nuevo llamado de atencion, recuerda diligenciarlo");
        javaMailSender.send(message);
    }

    /*
     * this method  is used to find all attentionsCalled with their  correspondent pagination
     * here there is two inputs, pageNumber corresponding to the number of the page to watch
     * and pageSize is the quantity of Called  for page  */
    @Override
    public Page<AttentionCalledDto> readLlamado(Integer pageSize, Integer pageNumber) {
        Pageable pageable = PageRequest.of( pageSize , pageNumber);
        return llamadoRepository.findAll(pageable)
                .map(LlamadoTransformer::getLlamadoDtoFromLlamado);
    }

    /**
     *
     * This method is in charge of saving the new data of the attention call that was filled out by the student */
    @Override
    public ResponseEntity<AttentionCalledDto> update(AttentionCalledDto attentionCalledDto) {
        AttentionCalled attentionCalled = LlamadoTransformer.getLlamadoFromLlamadoDto(attentionCalledDto);
        attentionCalled.setId(attentionCalledDto.getId());
        attentionCalled.setState(true);
        return new ResponseEntity(LlamadoTransformer.getLlamadoDtoFromLlamado(llamadoRepository.save(attentionCalled)), HttpStatus.OK);
    }

    /**
     * this method delete an  attentionCalled with thei id  */
    @Override
    public void delete(Long  id) {
        llamadoRepository.deleteById(id);
    }


    /**
     * this method obtains all calls
     * */
    @Override
    public List<AttentionCalled> getAllCalls() {
        return llamadoRepository.findAll();
    }

    /**
    this method  search by id a attentionCalled*/
    @Override
    public Optional<AttentionCalled> getById(Long  id) {
        return llamadoRepository.findById(id);
    }

    @Override
    public List<AttentionCalled> getCallsByUser(String documentNumber) {
        Optional<DetailUser> detailUser = detailUserRepository.findByDocumentNumber(documentNumber);
        Users user = detailUser.get().getUser();
        return llamadoRepository.findByApprentice(user);
    }

}
