export interface IProgramInterface {
  code?: string;
  version: string;
  nameProgram: string;
  durationTrimesterLective: string;
  status: boolean;
  justification: string;
  durationTrimesterProductive: string;
  trainingType: string;
  trainingMode: string;
}
