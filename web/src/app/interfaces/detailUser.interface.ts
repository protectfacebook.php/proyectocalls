import {IUserInterface} from '../account/user.interface';

export interface IDetailUserInterface {
  id: number;
  documentNumber: string;
  phone: string;
  emailPersonal: string ;
  address: string ;
  initialsSchedule: string;
  gender: string;
  documentType: string;
  user: IUserInterface;
}
