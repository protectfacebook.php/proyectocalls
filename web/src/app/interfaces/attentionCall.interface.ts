import {IUserInterface} from '../account/user.interface';
import {IFichaInterface} from '../entities/fichas/ficha.interface';

export interface IAttentionCallInterface {
  id?: number;
  instructor: IUserInterface;
  apprentice: IUserInterface;
  creationDate?: string;
  ficha: IFichaInterface;
  state: boolean;
  situationDescription: string;
  resumeSituation: string;
  impact: string;
  effect: string;
  commitment: string;
}
