import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IRolInterface} from './rol.interface';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';
import {IUserInterface, IUserSafi} from './user.interface';
import {IDetailUserInterface} from '../interfaces/detailUser.interface';
import {requestOption} from '../utils/requestOption';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private http: HttpClient) { }

  getRols(): Observable<IRolInterface[]> {
    return  this.http.get<IRolInterface[]>(`${environment.END_POINT}/api/roles`)
      .pipe(map((res) => {
        return res;
      }));
  }
  createUser(user: IUserInterface): Observable<IUserInterface> {
    return  this.http.post<IUserInterface>(`${environment.END_POINT}/api/user`, user )
      .pipe(map((res) => {
        return res;
      }, error => {
        return error;
      }));
  }
  saveDetails(details: IDetailUserInterface): Observable<IDetailUserInterface>{
    return  this.http.post<IDetailUserInterface>(`${environment.END_POINT}/api/details`, details)
      .pipe(map((response) => {
        return response;
      }, error => error));
  }
  createUserSafi(userSafi: any ): Observable<any>{
    return  this.http.post<any>(`${environment.END_POINT}/api/user-safi`, userSafi)
      .pipe(map((response) => {
        return response;
      }, error => error));
  }

  getAllDetails(): Observable<IDetailUserInterface[]>{
    return  this.http.get<IDetailUserInterface[]>(`${environment.END_POINT}/api/get-details`)
      .pipe(map((res) => {
        return res;
      }, error => {
        return error;
      }));
  }
  getUserByDocument(req: any): Observable<IDetailUserInterface[]>{
    const parameters  =  requestOption(req);
    return this.http.get<IDetailUserInterface[]>
    (`${environment.END_POINT}/api/details-doc`, { params: parameters } )
      .pipe(map((res) => {
        return res;
      }));
  }
}
