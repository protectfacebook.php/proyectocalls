import {Component, Input, OnInit} from '@angular/core';
import {IUserInterface, IUserSafi} from '../user.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IDetailUserInterface} from '../../interfaces/detailUser.interface';
import {AccountService} from '../account.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-details-user',
  templateUrl: './details-user.component.html',
  styleUrls: ['./details-user.component.styl']
})
export class DetailsUserComponent implements OnInit {
  @Input() user: IUserInterface;
  detailsGroup: FormGroup;
  details: IDetailUserInterface;
  created = false;
  usuario = {
    login: '',
    passwordHash: '',
    firstName: '',
    lastName: '',
    email: '',
    imageUrl: '',
    documentNumber: '',
    phone: '',
    emailPersonal: '',
    address: '',
    initialsSchedule: '',
    gender: '',
    documentType: ''
  };
  constructor(private fb: FormBuilder,
              private accountService: AccountService,
              private router: Router) {
    this.detailsGroup = this.fb.group({
      documentNumber : ['', Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(12)
      ])],
      phone : ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10)
      ])],
      emailPersonal : ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      address : ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100)
      ])],
      initialsSchedule : ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(3)
      ])],
      gender : ['', Validators.required],
      documentType: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  saveDetails(): void {
    this.details = this.detailsGroup.value;
    this.usuario.login = this.user.login;
    this.usuario.firstName = this.user.firstName;
    this.usuario.lastName = this.user.lastName;
    this.usuario.passwordHash = this.user.passwordHash;
    this.usuario.email = this.user.email;
    this.usuario.imageUrl = this.user.imageUrl;
    //
    this.usuario.documentNumber = this.details.documentNumber;
    this.usuario.phone = this.details.phone;
    this.usuario.emailPersonal = this.details.emailPersonal;
    this.usuario.address = this.details.address;
    this.usuario.initialsSchedule = this.details.initialsSchedule;
    this.usuario.gender = this.details.gender;
    this.usuario.documentType = this.details.documentType;
    console.log(this.usuario);
    this.accountService.createUserSafi(this.usuario)
      .subscribe(res => {
        this.created = true;
        return res;
      }, error => {
        this.created = true;
        this.interval();
      });
  }

  interval(): void {
    setTimeout(() => {
      this.router.navigate(['/dashboard/home']);
      console.log('el tiempo funciona');
    }, 2000);
  }

}
