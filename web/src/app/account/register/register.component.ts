import { Component, OnInit } from '@angular/core';
import {IRolInterface} from '../rol.interface';
import {AccountService} from '../account.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IUserInterface} from '../user.interface';
import {IDetailUserInterface} from '../../interfaces/detailUser.interface';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.styl']
})
export class RegisterComponent implements OnInit {
  rols: IRolInterface[];
  user: IUserInterface;
  details: IDetailUserInterface[];
  saved = false;
  registerForm: FormGroup;
  constructor(private  accountService: AccountService,
              private fb: FormBuilder) {
    this.registerForm = fb.group({
        login : ['', Validators.compose([
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(10)
        ])],
        passwordHash : ['', Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(12)
        ])],
      firstName : ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.max(25)
      ])],
      lastName : ['', Validators.compose([
        Validators.required,
        Validators.minLength(3),
        Validators.max(50)
      ])],
      email : ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      imageUrl : ['', Validators.compose([
        Validators.required
      ])]
    });
  }

  ngOnInit(): void {
    this.accountService.getRols()
      .subscribe(res => {
        this.rols = res;
        console.log(res);
      });
    this.accountService.getAllDetails()
      .subscribe(res => {
        console.log(res);
        this.details = res;
      });
  }
  saveUser(){
    this.user = this.registerForm.value;
    console.log(this.user);
    this.saved = true;
    /*
    this.accountService.createUser(this.user)
      .subscribe((response) => {
        this.saved = true;
        return response;
      } ,(error) => {
        console.log(error);
    });*/
  }
}
