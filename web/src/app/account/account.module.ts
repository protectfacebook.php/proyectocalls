import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import { DetailsUserComponent } from './details-user/details-user.component';
import {InputSwitchModule} from 'primeng/inputswitch';

@NgModule({
  declarations: [RegisterComponent, LoginComponent, DetailsUserComponent],
  imports: [
    CommonModule,
    AccountRoutingModule,
    ReactiveFormsModule,
    InputSwitchModule
  ]
})
export class AccountModule { }
