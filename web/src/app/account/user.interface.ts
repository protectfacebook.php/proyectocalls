import {IRolInterface} from './rol.interface';

export interface IUserInterface {
  id?: number;
  login: string;
  passwordHash: string;
  firstName: string;
  lastName: string;
  email: string;
  imageUrl: string;
}
export  interface IUserSafi {
 login: string;
  passwordHash: string;
  firstName: string;
   lastName: string;
  email: string;
   imageUrl: string;
  documentNumber: string;
   phone: string;
   emailPersonal: string;
   address: string;
   initialsSchedule: string;
   gender: string;
  documentType: string;
}
