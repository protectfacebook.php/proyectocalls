import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path : 'account',
    loadChildren: () => import('../app/account/account.module')
      .then(m => m.AccountModule)
  },
  {
    path : 'dashboard',
    loadChildren: () => import('../app/layouts/layouts.module')
      .then(m => m.LayoutsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
