import { Component, OnInit } from '@angular/core';
import {FichaService} from '../ficha.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {IFichaInterface} from '../ficha.interface';
import {IProgramInterface} from '../../../interfaces/program.interface';

@Component({
  selector: 'app-create-ficha',
  templateUrl: './create-ficha.component.html',
  styleUrls: ['./create-ficha.component.styl']
})
export class CreateFichaComponent implements OnInit {
  listFichas: IFichaInterface[];
  fichaGroup: FormGroup;
  programs: IProgramInterface[];
  constructor(private fichaService: FichaService,
              private fb: FormBuilder) {
    this.fichaGroup = this.fb.group({
      code: ['', Validators.required],
      startDate: ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(10)
      ])],
      state : false,
      dateEndLective : ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(10)
      ])],
      dateEndPractice : ['', Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.minLength(10)
      ])],
      offerType : '',
      trainingType: '',
      workingDay : '',
      emailLastQuarter: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])]
    });
  }

  ngOnInit(): void {
    this.fichaService.getPrograms()
      .subscribe(res => {
        this.programs = res;
      }, error => console.error(error));
  }
  initPagination(page: number): void{
    this.fichaService.getAllFichasPaginated({
      "pageNumber" : page,
      "pageSize" : 10
    })
      .subscribe((res: any ) => {
          this.listFichas = res.content;
      }, error => {
        console.log(error);
        return error;
      });
  }

  createFicha(): void{
    this.fichaService.createFicha(this.fichaGroup.value)
      .subscribe(res => {
        console.log(res);
        return res;
      }, error => console.log(error))
    ;
  }
  guardar(): void {
    console.log(this.fichaGroup.value);
  }
  valor(value: any): void {
    console.log(value);
  }

}
/*
*  code : '',
        version: '',
        nameProgram: '',
        durationTrimesterLective: '',
        durationTrimesterProductive: 0,
        trainingType: '',
        trainingMode: ''*/
