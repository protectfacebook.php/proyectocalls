import {IProgramInterface} from '../../interfaces/program.interface';

export interface IFichaInterface {
  id?: string;
  code?: string;
  startDate: string;
  state: boolean;
  dateEndLective: string;
  dateEndPractice: string;
  offerType: string;
  trainingType: string;
  workingDay: string;
  emailLastQuarter: string;
  program?: IProgramInterface;
  // private Program program;
}
