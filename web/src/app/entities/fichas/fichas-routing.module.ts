import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CreateFichaComponent} from './create-ficha/create-ficha.component';


const routes: Routes = [
  {
    path: 'create-ficha',
    component: CreateFichaComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FichasRoutingModule { }
