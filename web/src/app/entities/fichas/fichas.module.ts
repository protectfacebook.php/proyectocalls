import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {AutoCompleteModule} from 'primeng/autocomplete';
import { FichasRoutingModule } from './fichas-routing.module';
import { CreateFichaComponent } from './create-ficha/create-ficha.component';
import {ReactiveFormsModule} from '@angular/forms';
import {InputSwitchModule} from 'primeng/inputswitch';


@NgModule({
  declarations: [CreateFichaComponent],
  imports: [
    CommonModule,
    FichasRoutingModule,
    ReactiveFormsModule,
    InputSwitchModule,
    AutoCompleteModule
  ]
})
export class FichasModule { }
