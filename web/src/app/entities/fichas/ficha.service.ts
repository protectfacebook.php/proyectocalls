import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IFichaInterface} from './ficha.interface';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';
import {requestOption} from '../../utils/requestOption';
import {IProgramInterface} from '../../interfaces/program.interface';

@Injectable({
  providedIn: 'root'
})
export class FichaService {

  constructor(private  http: HttpClient) {
  }

  getAllFichas(): Observable<IFichaInterface[]> {
    return this.http.get<IFichaInterface[]>(`${environment.END_POINT}/api/get-fichas`)
      .pipe(map(res => res, error => error));
  }

  createFicha(ficha: IFichaInterface): Observable<IFichaInterface> {
    return this.http.post<IFichaInterface>(`${environment.END_POINT}/api/ficha`, ficha)
      .pipe(map((res) => {
        return res;
      }, error => error));
  }

  getAllFichasPaginated(req?: any): Observable<IFichaInterface[]> {
    const parameters = requestOption(req);
    return this.http.get<IFichaInterface[]>(`${environment.END_POINT}/api/ficha`, {params: parameters})
      .pipe(map(res => res, error => error));
  }
  getFichaByCode(req?: any ): Observable<IFichaInterface[]> {
    const parameters = requestOption(req);
    return this.http.get<IFichaInterface[]>(`${environment.END_POINT}/api/ficha/search`, {params: parameters})
      .pipe(map(res => res, error => error));
  }
  getProgramsByName(req: any): Observable<IProgramInterface[]>{
    const parameters = requestOption(req);
    return this.http.get<IProgramInterface[]>(`${environment.END_POINT}/api/program-name`,{params: parameters} )
      .pipe(map((res) => {
        return res;
      }, error => error));
  }
  getPrograms(): Observable<IProgramInterface[]>{
    return this.http.get<IProgramInterface[]>(`${environment.END_POINT}/api/programs` )
      .pipe(map((res) => {
        return res;
      }, error => {
        return error;
      }));
  }
}
