import { Injectable } from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DetailsUserComponent} from '../../account/details-user/details-user.component';
import {environment} from '../../../environments/environment';
import {requestOption} from '../../utils/requestOption';
import {map} from 'rxjs/operators';
import {IDetailUserInterface} from '../../interfaces/detailUser.interface';
import {IAttentionCallInterface} from '../../interfaces/attentionCall.interface';

@Injectable({
  providedIn: 'root'
})
export class CallsService {

  constructor(private http: HttpClient) { }

  getUserByInitials(req: any): Observable<IDetailUserInterface[]>{
    const parameters  =  requestOption(req);
    return this.http.get<IDetailUserInterface[]>
    (`${environment.END_POINT}/api/details-initials`, { params: parameters } )
      .pipe(map((res) => {
        return res;
      }));
  }
  getCallsByUserDocument(req: any): Observable<IAttentionCallInterface[]>{
    const parameters = requestOption(req);
    return this.http.get<IAttentionCallInterface[]>(`${environment.END_POINT}/api/attention-user`, {params: parameters})
      .pipe(map(res => res, error => error));
  }

  createCall(called: IAttentionCallInterface): Observable<IAttentionCallInterface>{
    return this.http.post<IAttentionCallInterface>(`${environment.END_POINT}/api/make-call`, called)
      .pipe(map((res) => {
        return res;
      }, error => error));
  }
}
