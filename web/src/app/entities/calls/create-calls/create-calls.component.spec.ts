import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateCallsComponent } from './create-calls.component';

describe('CreateCallsComponent', () => {
  let component: CreateCallsComponent;
  let fixture: ComponentFixture<CreateCallsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateCallsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateCallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
