import { Component, OnInit } from '@angular/core';
import {CallsService} from '../calls.service';
import {IDetailUserInterface} from '../../../interfaces/detailUser.interface';
import {element} from 'protractor';
import {IUserInterface} from '../../../account/user.interface';
import {FichaService} from '../../fichas/ficha.service';
import {IFichaInterface} from '../../fichas/ficha.interface';
import {AccountService} from '../../../account/account.service';
import {IAttentionCallInterface} from '../../../interfaces/attentionCall.interface';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import Swal from 'sweetalert2';
import {Router} from '@angular/router';
@Component({
  selector: 'app-create-calls',
  templateUrl: './create-calls.component.html',
  styleUrls: ['./create-calls.component.styl']
})
export class CreateCallsComponent implements OnInit {
  results: string[] = [];
  detailUsers: IDetailUserInterface[];
  selectedFicha: IFichaInterface;
  selectedInstructor: IUserInterface;
  selectedApprentice: IUserInterface;
  fichas: IFichaInterface[];
  usuarios: IDetailUserInterface[] = [];
  screens = {
    display: true,
    show: false,
    llamadoDisplay: false,
    fichaDisplay : false,
    selections : true
  };
  called = {
    instructor: this.selectedInstructor,
    apprentice: this.selectedApprentice,
    ficha: this.selectedFicha,
    state: false,
    situationDescription: '',
    resumeSituation: '',
    impact: '',
    effect: '',
    commitment: ''
  };
  constructor(private callsService: CallsService,
              private fichaService: FichaService,
              private accountService: AccountService,
              private fb: FormBuilder,
              private  router: Router) {}

  ngOnInit(): void {}

  createCall(): void {
    console.log('Datos reales' + this.called.impact);
    this.callsService.createCall({
      ficha: this.selectedFicha,
      apprentice: this.selectedApprentice,
      instructor: this.selectedInstructor,
      state: false,
      situationDescription : '',
      resumeSituation : '',
      impact : '',
      effect : '',
      commitment : ''
    })
      .subscribe((res) => {
        Swal.fire('El Llamado de atencion ha sido creado', 'Gracias por preferirnos', 'success')
          .then((result) => {
            if (result.value){
              this.router.navigate(['/dashboard/home']);
            }
          });

      }, error => error);
  }

  searchDocument(event: any): void {
    this.accountService.getUserByDocument({
      "documentNumber": event.query
    }).subscribe((res) => {
      this.detailUsers = res;
    });
  }

  searchFicha(event: any): void {
    this.fichaService.getFichaByCode({
      "code" : event.query
    }).subscribe((res) => {
      this.fichas = res;
    });
  }
  search(event: any) {
    this.callsService.getUserByInitials({
      'initialsSchedule': event.query
    })
      .subscribe((res) => {
        this.detailUsers = res;
        const details = this.detailUsers;
        details.forEach(detail => {
          this.results.push(detail.initialsSchedule);
        });
      });
  }

  saveFicha(ficha: IFichaInterface){
    this.called.ficha = ficha;
    this.screens.llamadoDisplay = true;
    this.selectedFicha = ficha;
  }
  saveUser(user: IUserInterface, detalles: IDetailUserInterface): void {
    this.screens.display = false;
    if (this.usuarios.length >= 1) {
      this.usuarios.push(detalles);
      this.selectedApprentice = user;
      this.called.apprentice = user;
      this.screens.show = true;
      this.screens.selections = false;
    } else {
      this.selectedInstructor = user;
      this.called.instructor = user;
      this.usuarios.push(detalles);
      this.results = [];
      this.detailUsers = [];
    }
  }
}
