import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DialogModule} from 'primeng/dialog';
import { CallsRoutingModule } from './calls-routing.module';
import { ListCallsComponent } from './list-calls/list-calls.component';
import { CreateCallsComponent } from './create-calls/create-calls.component';
import {AutoCompleteModule} from 'primeng/autocomplete';
import {InputSwitchModule} from 'primeng/inputswitch';
import {FormsModule} from '@angular/forms';
@NgModule({
  declarations: [ListCallsComponent, CreateCallsComponent],
  imports: [
    CommonModule,
    CallsRoutingModule,
    AutoCompleteModule,
    InputSwitchModule,
    FormsModule
  ]
})
export class CallsModule { }
