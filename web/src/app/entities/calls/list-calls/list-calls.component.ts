import { Component, OnInit } from '@angular/core';
import {IAttentionCallInterface} from '../../../interfaces/attentionCall.interface';
import {CallsService} from '../calls.service';

@Component({
  selector: 'app-list-calls',
  templateUrl: './list-calls.component.html',
  styleUrls: ['./list-calls.component.styl']
})
export class ListCallsComponent implements OnInit {
  constructor(private callsService: CallsService) { }
  listCalls: IAttentionCallInterface[];
  selectedCall : IAttentionCallInterface;
  documento: string = null;

  ngOnInit(): void {
  }

  getCallsByDocumentUser(): void {
    this.callsService.getCallsByUserDocument(
      {
        "documentNumber" : this.documento
      }
    ).subscribe(res => {this.listCalls = res; }, error => console.log(error));
  }
  addCall(call: IAttentionCallInterface): void {
    this.selectedCall = call;
  }
}
