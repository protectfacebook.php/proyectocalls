import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListCallsComponent} from './list-calls/list-calls.component';
import {CreateCallsComponent} from './create-calls/create-calls.component';


const routes: Routes = [
  {
    path: 'list',
    component: ListCallsComponent
  },
  {
    path: 'create',
    component: CreateCallsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CallsRoutingModule { }
