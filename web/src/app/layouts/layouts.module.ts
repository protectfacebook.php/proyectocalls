import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { MainComponent } from './main/main.component';
import {LayoutsRoutingModule} from './layouts-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import {CallsModule} from '../entities/calls/calls.module';
import {FichasModule} from '../entities/fichas/fichas.module';
import { WelcomeComponent } from './welcome/welcome.component';



@NgModule({
  declarations: [HomeComponent, MainComponent, WelcomeComponent],
  imports: [
    CommonModule,
    LayoutsRoutingModule,
    CallsModule,
    FichasModule
  ]
})
export class LayoutsModule { }
