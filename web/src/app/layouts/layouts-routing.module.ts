import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './main/main.component';
import {HomeComponent} from './home/home.component';
import {LoginComponent} from '../account/login/login.component';
import {RegisterComponent} from '../account/register/register.component';
import {CreateCallsComponent} from '../entities/calls/create-calls/create-calls.component';
import {ListCallsComponent} from '../entities/calls/list-calls/list-calls.component';
import {CreateFichaComponent} from '../entities/fichas/create-ficha/create-ficha.component';
import {WelcomeComponent} from './welcome/welcome.component';


const routes: Routes = [
  {
    path: 'main',
    component: MainComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    children : [
      {
        path: '',
        component: WelcomeComponent
      },
      {
        path: 'list',
        component: ListCallsComponent
      },
      {
        path: 'create',
        component: CreateCallsComponent
      },
      {
        path: 'create-ficha',
        component: CreateFichaComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutsRoutingModule { }
