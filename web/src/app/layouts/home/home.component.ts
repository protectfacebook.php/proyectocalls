import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.styl']
})
export class HomeComponent implements OnInit {
  display = true ;
  constructor() { }

  ngOnInit(): void {
  }

  closeSide(): void {
    if (this.display ===  true){
      this.display = false;
    }else{
     this.display = true;
    }
  }
}
